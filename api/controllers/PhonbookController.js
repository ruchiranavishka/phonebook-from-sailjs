/**
 * PhonbookController
 *
 * @description :: Server-side logic for managing phonbooks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	
	AddContactDetails: function (req, res) {


    Contactserv.AddContactDetails({name: req.param('name'),phoneNumber:req.param('number')},function(err,message) {
    if (!err) {
      return res.send(message);
    }else{
      return res.notFound();
    }
   });
	},   


   GetContacts: function (req,res) {
    
    Contactserv.GetContacts(function(err, records) {
    if (!err) {
      return res.json(records);
    }else{
      return res.notFound();
    }
    });
    
  },
 
};

